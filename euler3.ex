defmodule Euler3 do
	def maxPrimeFactor(num, divisor, factors) when num <= 1 do
		List.first(factors)
	end

	def maxPrimeFactor(num, divisor, factors) when num > 1 and rem(num, divisor) == 0 do
		maxPrimeFactor(div(num, divisor), divisor, [divisor|factors])
	end

	def maxPrimeFactor(num, divisor, factors) when num > 1 do
		maxPrimeFactor(num, divisor + 1, factors)
	end

	def maxPrimeFactor(num) do
		maxPrimeFactor(num, 2, [])
	end
end

IO.inspect Euler3.maxPrimeFactor(600851475143)
