defmodule Euler6 do
	def sum_squares(num, accum) do
    if num <= 100 do
		  sum_squares(num + 1, accum + (num * num))
	  else
      accum
    end
	end

	def sum(num, accum) do
   if num <= 100 do
		sum(num + 1, accum + num)
	 else
		accum
	 end
  end

	def square_sum() do
		result = sum(1, 0)
		result * result
	end

	def solve() do
		(square_sum() - sum_squares(1, 0))
	end
end

IO.puts Euler6.solve()
