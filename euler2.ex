defmodule Euler2 do
    def fibonacci_even_sum do
        Stream.unfold({1, 2}, fn({a, b}) -> if a > 4_000_000, do: nil, else: {a, {b, a + b}} end)
        |> Enum.reduce(0, fn(a, acc) -> if rem(a, 2) == 0, do: a + acc, else:  acc end)
    end
end

IO.inspect Euler2.fibonacci_even_sum
