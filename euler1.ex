defmodule Euler1 do
  def sum(0), do: 0
  def sum(n) when rem(n-1,3) == 0 or rem(n-1,5) == 0, do: n-1 + sum(n-1)
  def sum(n), do: sum(n-1)
end
IO.puts Euler1.sum(1000)
